<?php
    include "../db/connect.php";

    // Deuxième vérification sur le bon remplissage des informations
    if(empty($_POST["mdp"]) || empty($_POST["email"])){
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><b>Veuillez remplir tous les champs</b>
			</div>
		";
		exit();
    }
    else{
        // Récupération des variables postées
        $email = $_POST["email"];
        $mdp = $_POST["mdp"];
        $sql = "SELECT * FROM `user_info` WHERE `email` = '$email'";
        $mdpResult = mysqli_query($con, $sql);
        $resultat = mysqli_fetch_array($mdpResult);
        $verifmdp = password_verify($mdp, $resultat['password']);
        // Vérification de l'existence de l'email
        if (!$resultat){
            ?> <script> alert("L'identifiant ou le mot de passe est eronné !"); </script> <?php
            echo "<script> location.href='./login.php'; </script>";
        }
        // Vérification de l'existence du mot de passe
        else{
            if ($verifmdp) {
                session_start();
                $_SESSION["email"] = $email;
                echo "<script> location.href='./gesteventsadmin.php'; </script>";
            }
            else {
                ?> <script> alert("L'identifiant ou le mot de passe est eronné !"); </script> <?php
                echo "<script> location.href='./loginForm.php'; </script>";
            }
        }
    }
?>