<!DOCTYPE html>
<html>
<head>
    <title> Liste des évènements </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- A noter dans la doc technique -->
    <link rel="icon" type="image/png" href="../images/see.svg" />
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
    <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../css/demo.css" />
</head>
    <body>
        <div class="container">
        <?php
		header('Content-Type: text/html; charset=utf-8');

                include "../db/connect.php";
                include "./session_verify.php";
                ?> <div class="container"><?php include '../includes/menu.php'; ?> </div>
        <?php   
                $sql = "SELECT * FROM events";
                $query = mysqli_query($con, $sql);
                echo '<h1>Liste des évènements</h1>';
                echo '<table class="table table-sm table-bordered">
                    <thead class="table-primary">
                        <tr>
                            <td>Titre</td>
                            <td>Description</td>
                            <td>Date de début</td>
                            <td>Date de fin</td>
                            <td>Heure de début</td>
                            <td>Heure de fin</td>
                            <td>Participants maximum</td>
                            <td>Actions</td>
                        <tr>
                    </thead>
                    <tbody class="table-info">'
                ;
                while($data = mysqli_fetch_assoc($query)){
                    $firstHour = substr($data["heure_debut"], 0, 5);
                    $lastHour = substr($data["heure_fin"], 0, 5);
            ?>
                <tr style="text-align:justify">
                    <td><?php echo $data["event_title"]; ?></td>
                    <td><?php echo $data["event_description"]; ?></td>
                    <td><?php echo $data["start_date"]; ?></td>
                    <td><?php echo $data["last_date"]; ?></td>
                    <td><span><?php echo $firstHour; ?></span></td>
                    <td><span><?php echo $lastHour; ?></span></td>
                    <td><?php echo $data["max_participents"]; ?></td>
                    <td><a href='participents.php?id=<?php echo $data["event_id"]; ?>'><img src="../images/see.svg" width="18" height="18"></a>&nbsp<a href='deleteEvent.php?id=<?php echo $data["event_id"]; ?>'><img src="./images/delete.svg" width="18" height="18"></a></td>
                </tr>
            <?php } ?>
            </tbody>
            </table>
    </body>
</html>
<?php 
    include "./cssjs/css.php";
    include "./cssjs/js.php";
?>
