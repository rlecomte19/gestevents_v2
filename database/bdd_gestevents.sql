-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 07 avr. 2021 à 06:15
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdd_gestevents`
--

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(100) NOT NULL AUTO_INCREMENT,
  `event_title` text NOT NULL,
  `event_description` varchar(1500) NOT NULL,
  `start_date` date NOT NULL,
  `last_date` date NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `max_participents` int(100) NOT NULL,
  `first_reserv` date NOT NULL,
  `last_reserv` date NOT NULL,
  `location` varchar(60) NOT NULL,
  `img_link` text NOT NULL,
  `type_id` int(100) NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `eventsForeign` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `events`
--

INSERT INTO `events` (`event_id`, `event_title`, `event_description`, `start_date`, `last_date`, `heure_debut`, `heure_fin`, `max_participents`, `first_reserv`, `last_reserv`, `location`, `img_link`, `type_id`) VALUES
(1, 'Etre acteur de l’adaptation au nouveau régime climatique', 'Cet évènement est organisé dans le cadre du partenariat avec le Club CNRS Jeunes Sciences et citoyens à destination des lycéens délégués au Développement Durable du lycée Laetitia Bonaparte d’Ajaccio.\r\nEtre acteur de l’adaptation au nouveau régime climatique, tel est le thème de cette rencontre mêlant projection de films de témoignages d’agriculteurs, d’éleveurs confrontés aux bouleversements climatiques.\r\nCe projet est porté par le CPIE d’Ajaccio accompagné d’un comité de pilotage (Université de Corse, INRA, OEC, PNRC).\r\nLe débat en présence d’experts (laboratoire SPE 6134 CNRS/Université de Corse, éco-citoyens jardiniers,..), doit permettre de partager avec les lycéens les interrogations et les moyens d’agir.', '2020-01-30', '2020-02-04', '16:00:00', '18:00:00', 20, '2020-01-25', '2020-01-28', 'Lycée Laetitia Bonparte', 'unset.png', 3),
(2, 'Ciné-club du mercredi 18 décembre 2019', 'Pour terminer l\'année 2019 avec le sourire, nous proposons aux élèves mercredi 18 décembre de 16 à 18h, en salle Robin Renucci, une petite perle de film drôle. Il s\'agit de \"The Party\"  de Blake Edwards et joué par Peter Sellers (célèbre acteur des films \"La panthère rose\") : Hrundi V. Bakshi (Peter Sellers), [...]', '2019-12-18', '2019-12-18', '16:00:00', '18:00:00', 30, '2019-12-12', '2019-12-15', 'Lyce Laetitia Bonaparte', 'unset.jpg', 3);

-- --------------------------------------------------------

--
-- Structure de la table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
CREATE TABLE IF NOT EXISTS `event_type` (
  `type_id` int(10) NOT NULL AUTO_INCREMENT,
  `type_title` text NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `event_type`
--

INSERT INTO `event_type` (`type_id`, `type_title`) VALUES
(1, 'Technical Events'),
(2, 'Gaming Events'),
(3, 'On Stage Events'),
(4, 'Off Stage Events'),
(5, 'new event Events');

-- --------------------------------------------------------

--
-- Structure de la table `participent`
--

DROP TABLE IF EXISTS `participent`;
CREATE TABLE IF NOT EXISTS `participent` (
  `user_id` int(15) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_events_particip` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `participent`
--

INSERT INTO `participent` (`user_id`, `fullname`, `email`, `event_id`) VALUES
(23, 'Roman Lecomte', 'romanlecomte@gmail.com', 2);

-- --------------------------------------------------------

--
-- Structure de la table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(120) NOT NULL,
  `password` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_info`
--

INSERT INTO `user_info` (`user_id`, `email`, `password`) VALUES
(5, 'romanlecomte@gmail.com', '$2y$10$z.Or333wBFygwmEJsiN/6uwSJD7VVy56567L30iYQDAumIcYcU19.');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `eventsForeign` FOREIGN KEY (`type_id`) REFERENCES `event_type` (`type_id`);

--
-- Contraintes pour la table `participent`
--
ALTER TABLE `participent`
  ADD CONSTRAINT `fk_events_particip` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
