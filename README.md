# gest_events
Application web permettant à des utilisateurs de s'inscrire à des évènements

# Installation

git clone https://gitlab.com/siollb/gestevents.git 
    
Créer une base de données vide nommée "bdd_gestevents"

Importer le SQL "events.sql" disponible dans le dossier "database"

Créer un utilisateur MySQL autorisé à accéder à la nouvelle base de données

Configurer le fichier "db/connect.php" avec les informations de connexion au serveur de base de données

L'application est maintenant fonctionnelle